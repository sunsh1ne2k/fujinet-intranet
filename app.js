require('dotenv').config()
var createError = require("http-errors");
var cors = require("cors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");

var indexRouter = require("./routes/index");
var tagRouter = require("./routes/tags"); // import routes for Tags
var categoryRouter = require("./routes/categories"); //import routes for Categories
var contentRouter = require("./routes/contents"); //import routes for Content
var roleRouter = require("./routes/roles"); // import routes for Role
var authRouter = require("./routes/auth"); // import routes for Auth
var uploadRouter = require("./routes/upload"); // import routes for Upload

var app = express();

var allowed_host = {
  localhost: "http://localhost:8081",
}

var mongoose = require("mongoose");
mongoose.connect(process.env.MONGODB_URL, { useNewUrlParser: true, useUnifiedTopology: true });
var db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error"));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

app.use("/", indexRouter);
app.use("/api", tagRouter, roleRouter, categoryRouter);
app.use("/api", authRouter, uploadRouter);
app.use("/api", contentRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

module.exports = app;
