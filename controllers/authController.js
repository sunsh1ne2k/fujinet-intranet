const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/user');

exports.user_register = async function (req, res){
    try{
        const salt = await bcrypt.genSalt(10);
        const hashedPassword = await bcrypt.hash(req.body.password, salt)

        const user = new User({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            username: req.body.username,
            email: req.body.email,
            password: hashedPassword
        })

        const latest_user = await user.save();
        await res.status(200).json(latest_user)
    }
    catch(error){
        res.status(400).json({message: error.message})
    }
}

exports.user_login = async function (req, res){
    const user = await User.findOne({email: req.body.email})
    if (!user) return res.status(422),json({message: "Invalid email or password!"})

    // if email already exists.
    const isValidPassword = await bcrypt.compare(req.body.password, user.password)
    if (!isValidPassword) return res.status(422).json("Invalid password!")
   
    const token = jwt.sign({_id: user._id}, process.env.JWT_SECRET_KEY, {expiresIn: 60*60*24}) // 24 hours

    res.header('auth-token', token).json(token);
    // return res.json(`User ${user.email} has logged in`);
}

exports.user_list = async function (req, res){
    try {
        const queryset = await User.find()
        res.json(queryset)
    }
    catch(error){
        res.status(500).json({ message: error.message })
    }
}
