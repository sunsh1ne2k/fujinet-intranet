var Category = require("../models/category");

// create new Category
exports.category_create = async function (req, res) {
  const data = new Category({
    name: req.body.name,
    icon: req.body.icon
  });

  try {
    const latest_category = await data.save();
    res.status(200).json(latest_category);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// retrieve all Categories
exports.category_list = async function (req, res) {
  try {
    const queryset = await Category.find();
    res.json(queryset)
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// retrive a specific Category
exports.category_retrieve = async function (req, res) {
  try {
    const queryset = await Category.findById(req.params.id);
    res.json(queryset)
  }
  catch (error) {
    res.status(500).json({ message: error.message })
  }
}

// update a specific Category
exports.category_update = async function (req, res) {
  try {
    const id = req.params.id;
    const updated_data = req.body;
    const options = {new: true};

    const result = await Category.findByIdAndUpdate(
      id, updated_data, options
    )

    res.send(result)
  }
  catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// deleate a specific Category
exports.category_delete = async function (req, res) {
  try{
    const id = req.params.id;
    const object = await Category.findByIdAndDelete(id);
    res.send(`Category document with name: ${object.name} has been deleted`)
  }
  catch (error) {
    res.status(400).json({ message: error.message });
  }
};



