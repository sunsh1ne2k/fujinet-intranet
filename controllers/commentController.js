var Comment = require("../models/comment");

exports.comment_create  = async function(req, res) {
  try {
    const comment = new Comment(req.body);
    const latest_comment = await comment.save();
    res.status(200).json(latest_comment);
  } catch (errror) {
    res.status(400).json({message: errror.message});  
  }
};

exports.comment_list = async function (req, res) {
  try{
    const queryset = await Comment.find();
    res.json(queryset);
  }
  catch (error) {res.status(400).json({message: error.message});}
}

exports.comment_retrieve = async function (req, res) {
  try {
    const queryset = await Comment.findById(req.params.id);
    res.json(queryset);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.comment_update = async function (req, res) {
  try {
    const id = req.params.id;
    const updated_data = req.body;
    const options = { new: true };

    const result = await Comment.findByIdAndUpdate(id, updated_data, options);

    res.send(result);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

exports.comment_delete = async function (req, res) {
  try {
    const id = req.params.id;
    const object = await Comment.findByIdAndDelete(id);
    res.send(`Comment with content ${object.title} has been deleted`)
  }
  catch (error) {
    res.status(400).json({ message: error.message})
  }
}