const res = require("express/lib/response");
const content = require("../models/content");
var Content = require("../models/content");

// display list of all contents
exports.content_create = async function (req, res) {
  try {
    console.log(req.body);
    const content = new Content(req.body);
    const latest_content = await content.save();
    res.status(200).json(latest_content);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

exports.content_retrieve = async function (req, res) {
  try {
    const queryset = await Content.findById(req.params.id)
      .populate({
        path: "tags",
        select: ["name"],
      })
      .populate({
        path: "category",
        select: ["name"],
      })
      .populate({
        path: "comments",
        populate: {
          path: "author",
          select: ["first_name", "last_name", "username"]
        }
      })
      .populate({
        path: "reactions",
        populate: {
          path: "author",
          select: ["first_name", "last_name", "username"]
        }
      })
      .populate({
        path: "author",
        select: ["first_name", "last_name", "username"],
      });
    res.json(queryset);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// display content create from on GET.
exports.content_update = async function (req, res) {
  try {
    const id = req.params.id;
    const updated_data = req.body;
    const options = { new: true };

    const result = await Content.findByIdAndUpdate(id, updated_data, options);

    res.send(result);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

exports.content_list = async function (req, res) {
  try {
    const queryset = await Content.find();
    res.json(queryset);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

exports.content_delete = async function (req, res) {
  try {
    const id = req.params.id;
    const object = await Content.findByIdAndDelete(id);
    res.send(`Content with title ${object.title} has been deleted`);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};
