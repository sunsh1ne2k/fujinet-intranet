require("dotenv").config();
const multer = require("multer");
const multerS3 = require("multer-s3");
const AWS = require("aws-sdk");
const crypto = require("crypto");

const s3Config = new AWS.S3({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  Bucket: process.env.AWS_STORAGE_BUCKET_NAME,
});

const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === "image/jpeg" ||
    file.mimetype === "image/png" ||
    file.mimetype === "image/gif" ||
    file.mimetype === "image/jpg" ||
    file.mimetype === "image/webp"
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const storage = multer.diskStorage({
  destination: (req, res, cb) => {
    cb(null, "media/images");
  },
  filename: (req, file, cb) => {
    cb(null, new Date().toISOString + "-" + file.originalname);
  },
});

const multerS3Config = multerS3({
    contentType: multerS3.AUTO_CONTENT_TYPE,
    acl: "public-read",
    s3: s3Config,
    bucket: process.env.AWS_STORAGE_BUCKET_NAME,
    metadata: function (req, file, cb) {
        cb(null, {fieldName: file.fieldname});
    }, 
    key: function(req, file, cb) {
        console.log(file)

        cb(null, new Date().toISOString + '-' + file.originalname)
    }
})

const upload = multer({
    storage: multerS3Config,
    fileFilter: fileFilter,
    limits: { 
        fileSize: 1024 * 1024 * 5 // 5MB 
    }
})

// const upload = multer({
//     storage: multerS3({
//         s3: s3, 
//         bucket: process.env.AWS_STORAGE_BUCKET_NAME,
//         metadata: function(req, file, cb){
//             cb(null, {fieldName: file.fieldname});
//         },
//         key: function(req, file, cb){
//             cb(null, new Date().toISOString());
//         }
//     })
// })

exports.imageUploader = upload;