var Reaction = require("../models/reaction");

// display list of all reactions
exports.reaction_list = function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction list");
};

// display detail page for a specific reaction
exports.reaction_detail = function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction detail");
};

// display reaction create from on GET.
exports.reaction_create_get = function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction create GET");
};

// handle reaction create on POST.
exports.reaction_create_post = function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction create POST");
};

// display reaction delete from on GET.
exports.reaction_delete_get = function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction delete GET");
};

// display reaction delete from on POST.
exports.reaction_delete_post = function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction delete POST");
};

// display reaction update from GET.
exports.reaction_update_get = function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction update GET");
};

// handle reaction update POST
exports.reaction_update_post = (function (req, res) {
  res.send("NOT IMPLEMENTED: Reaction update POST");
});