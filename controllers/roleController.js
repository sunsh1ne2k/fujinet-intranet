var Role = require("../models/role");

// create new role
exports.role_create = async function (req, res) {
  const data = new Role({
    name: req.body.name,
  });

  try {
    const latest_role = await data.save();
    res.status(200).json(latest_role);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// retrieve all roles
exports.role_list = async function (req, res) {
  try {
    const queryset = await Role.find();
    res.json(queryset)
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// retrive a specific role
exports.role_retrieve = async function (req, res) {
  try {
    const queryset = await Role.findById(req.params.id);
    res.json(queryset)
  }
  catch (error) {
    res.status(500).json({ message: error.message })
  }
}

// update a specific role
exports.role_update = async function (req, res) {
  try {
    const id = req.params.id;
    const updated_data = req.body;
    const options = {new: true};

    const result = await Role.findByIdAndUpdate(
      id, updated_data, options
    )

    res.send(result)
  }
  catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// deleate a specific role
exports.role_delete = async function (req, res) {
  try{
    const id = req.params.id;
    const object = await Role.findByIdAndDelete(id);
    res.send(`role document with name: ${object.name} has been deleted`)
  }
  catch (error) {
    res.status(400).json({ message: error.message });
  }
};



