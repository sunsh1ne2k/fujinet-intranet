var Tag = require("../models/tag");

// create new Tag
exports.tag_create = async function (req, res) {
  const data = new Tag({
    slug: req.body.slug,
    name: req.body.name,
  });

  try {
    const latest_tag = await data.save();
    res.status(200).json(latest_tag);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// retrieve all Tags
exports.tag_list = async function (req, res) {
  try {
    const queryset = await Tag.find();
    res.json(queryset)
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// retrive a specific tag
exports.tag_retrieve = async function (req, res) {
  try {
    const queryset = await Tag.findById(req.params.id);
    res.json(queryset)
  }
  catch (error) {
    res.status(500).json({ message: error.message })
  }
}

// update a specific Tag
exports.tag_update = async function (req, res) {
  try {
    const id = req.params.id;
    const updated_data = req.body;
    const options = {new: true};

    const result = await Tag.findByIdAndUpdate(
      id, updated_data, options
    )

    res.send(result)
  }
  catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// deleate a specific Tag
exports.tag_delete = async function (req, res) {
  try{
    const id = req.params.id;
    const object = await Tag.findByIdAndDelete(id);
    res.send(`Tag document with name: ${object.name} has been deleted`)
  }
  catch (error) {
    res.status(400).json({ message: error.message });
  }
};



