var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var CategorySchema = new Schema({
  name: { type: String, required: true, unique: true},
  icon: {type: String, required: true}
});

CategorySchema.virtual("url").get(function () {
  return "backup/category/" + this._id;
});

// export model
module.exports = mongoose.model("Category", CategorySchema);
