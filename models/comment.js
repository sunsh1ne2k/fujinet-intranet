var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var CommentSchema = new Schema({
  // content: { type: Schema.Types.ObjectId, ref: "Content", required: true},
  image: { type: String },
  author: { type: Schema.Types.ObjectId, ref: "User", required: true},
  parent: { type: Schema.Types.ObjectId, ref: "Comment", default: null},
  comment_content: { type: String, required: true},
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
});

// virtual for comment's url
CommentSchema.virtual("url").get(function () {
  return "backup/url/" + this._id;
});

// export model
module.exports = mongoose.model("Comment", CommentSchema);
