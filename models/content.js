var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var CommentSchema = require("./comment").schema
var ReactionSchema = require("./reaction").schema

var ContentSchema = new Schema({
  reactions: [ReactionSchema],
  comments: [CommentSchema],
  tags: [{ type: Schema.Types.ObjectId, ref: "Tag" }],
  category: { type: Schema.Types.ObjectId, ref: "Category", required: true },
  author: { type: Schema.Types.ObjectId, ref: "User", required: true },
  slug: { type: String, required: true, unique: true },
  title: { type: String, required: true, unique: true },
  cover_image: {
    type: String,
  },
  content: {
    type: String,
    get: function(data) {
      try {
        return JSON.parse(data);
      } catch (error) {
        return data;
      }
    },
    set: function(data){
      return JSON.stringify(data);
    },
  },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  magazine_link: { type: String },
  status: {
    type: String,
    enum: ["DRAFT", "POSTED", "DELETED", "PERMANENTLY DELETED"],
    default: "DRAFT",
  },
});
// virtual for content's url
ContentSchema.virtual("url").get(function () {
  return "backup/url/" + this._id;
});

// export model
module.exports = mongoose.model("Content", ContentSchema);
