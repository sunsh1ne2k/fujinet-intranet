var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var ContentTemplateSchema = new Schema({
  slug: { type: String, required: true, unique: true },
  title: { type: String, required: true, unique: true },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  category: { type: Schema.Types.ObjectId, ref: "Category" },
});

// virtual for contenttemplate's url
ContentTemplateSchema.virtual("url").get(function () {
  return "backup/url/" + this._id;
});

// export model
module.exports = mongoose.model("ContentTemplate", ContentTemplateSchema);
