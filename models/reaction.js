var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var ReactionSchema = new Schema({
  // content: { type: Schema.Types.ObjectId, ref: "Content", required: true },
  author: { type: Schema.Types.ObjectId, ref: "User", required: true},
  type: { type: String, enum: ["LIKE", "LOVE", "CELEBRATE", "INSIGHTFUl"], required: true},
});

// virtual for reaction's url
ReactionSchema.virtual("url").get(function () {
  return "backup/url/" + this._id;
});

// export model
module.exports = mongoose.model("Reaction", ReactionSchema);
