var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var TagSchema = new Schema({
  slug: { type: String, required: true, unique: true},
  name: { type: String, required: true, unique: true},
});

TagSchema.virtual("url").get(function () {
  return "/backup/tag/" + this._id;
});

// export model
module.exports = mongoose.model("Tag", TagSchema);
