var mongoose = require("mongoose");

var Schema = mongoose.Schema;

var UserSchema = new Schema({
  password: { type: String, required: true },
  last_login: { type: Date, default: Date.now },
  first_name: { type: String, required: true },
  last_name: { type: String, required: true },
  date_joined: { type: Date, default: Date.now },
  username: {type: String, required: true, unique: true},
  email: { type: String, required: true, unique: true},
  bio: { type: String },
  birthday: { type: Date, default: Date.now },
  address: { type: String },
  avatar: { type: String },
  roles: [{ type: mongoose.Schema.Types.ObjectId, ref: "Role" }],
  save_posts: [{ type: Schema.Types.ObjectId, ref: "Content" }],
});

// virtual for user's fullname
UserSchema.virtual("name").get(function () {
  var fullname = "";
  if (this.first_name && this.last_name) {
    fullname = this.first_name + " " + this.last_name;
  }
  if (!this.first_name || !this.last_name) {
    fullname = "";
  }
  return fullname;
});

// virtual for user's url
UserSchema.virtual("url").get(function () {
  return "backup/url/" + this._id;
});

// export model
module.exports = mongoose.model("User", UserSchema);
