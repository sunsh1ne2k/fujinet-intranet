const express = require('express');

const router = express.Router();

const auth_controller = require('../controllers/authController');

const IsAuthenticated = require('../middleware/permissions');

router.post("/register", auth_controller.user_register)
router.post("/login", auth_controller.user_login)
router.get("/users", IsAuthenticated, auth_controller.user_list)

module.exports = router;