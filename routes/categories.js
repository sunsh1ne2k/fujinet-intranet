var express = require("express");

var router = express.Router();

var Category = require("../models/category")

var category_controller = require("../controllers/categoryController")

router.post("/categories", category_controller.category_create)
router.get("/categories", category_controller.category_list)
router.get("/categories/:id", category_controller.category_retrieve)
router.put("/categories/:id", category_controller.category_update)
router.delete("/categories/:id", category_controller.category_delete)

// export router.
module.exports = router;