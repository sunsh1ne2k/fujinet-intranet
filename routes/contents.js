var express = require("express");

var router = express.Router();

var Content = require("../models/content")

var content_controller = require("../controllers/contentController")

router.post("/content", content_controller.content_create)
router.get("/content", content_controller.content_list)
router.get("/content/:id", content_controller.content_retrieve)
router.put("/content/:id", content_controller.content_update)
router.delete("/content/:id", content_controller.content_delete)

// export router.
module.exports = router;