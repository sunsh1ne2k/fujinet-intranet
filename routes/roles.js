var express = require("express");

var router = express.Router();

var Role = require("../models/role")

var role_controller = require("../controllers/roleController")

router.post("/role", role_controller.role_create)
router.get("/role", role_controller.role_list)
router.get("/role/:id", role_controller.role_retrieve)
router.put("/role/:id", role_controller.role_update)
router.delete("/role/:id", role_controller.role_delete)

// export router.
module.exports = router;