var express = require("express");

var router = express.Router();

var Tag = require("../models/tag")

var tag_controller = require("../controllers/tagController")

router.post("/tags", tag_controller.tag_create)
router.get("/tags", tag_controller.tag_list)
router.get("/tags/:id", tag_controller.tag_retrieve)
router.put("/tags/:id", tag_controller.tag_update)
router.delete("/tags/:id", tag_controller.tag_delete)

// export router.
module.exports = router;