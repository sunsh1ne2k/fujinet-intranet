const express = require('express');

const {imageUploader} = require('../controllers/fileController')

const IsAuthenticated = require("../middleware/permissions")

const router = express.Router();

router.post("/upload", IsAuthenticated, imageUploader.single('photo'), function(req, res, next) {
    res.json({file: req.file, status: "uploaded!"})
})

module.exports = router;